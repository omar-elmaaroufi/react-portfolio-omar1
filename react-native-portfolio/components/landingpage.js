import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';


class Landing extends Component {
  render() {
    return(
      <div style={{width: '100%', margin: 'auto'}}>
        <Grid className="landing-grid">
          <Cell col={12}>
            <img
              src="https://media-exp1.licdn.com/dms/image/C4D03AQG3v412saring/profile-displayphoto-shrink_200_200/0?e=1586995200&v=beta&t=qplgiWmpMm_XG4XO-io-3XULcFBv7f8F2OVLzXCmDNE"
              alt="avatar"
              className="avatar-img"
              />

            <div className="banner-text">
              <h1>Oracle APEX Developer </h1>

            <hr/>

          <p>PL/SQL | Oracle APEX | SQL | HTML/CSS | JavaScript | React | React Native </p>

        <div className="social-links">

          {/* LinkedIn */}
          <a href="http://google.com" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-linkedin-square" aria-hidden="true" />
          </a>

          {/* Github */}
          <a href="http://google.com" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-github-square" aria-hidden="true" />
          </a>

        </div>
            </div>
          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Landing;
